# Download and build a suitable copy of Open Babel.
# The GROMACS team won't distribute source or binaries linked to Open Babel
# because we are choosing to be very clear about distributing only
# LGPL-licensed code, to suit requirements from our funding source.
#

# Machinery for running the external project
set(DOWNLOAD_DIR ${CMAKE_CURRENT_BINARY_DIR})
set(BUILD_DIR ${CMAKE_CURRENT_BINARY_DIR}/build)

include(ExternalProject)
ExternalProject_add(openbabel
    GIT_REPOSITORY git://github.com/openbabel/openbabel.git
    DOWNLOAD_DIR ${DOWNLOAD_DIR}
    INSTALL_COMMAND ""
    BINARY_DIR ${BUILD_DIR})
#    INSTALL_DIR ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})
